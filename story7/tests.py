from django.http import response
from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .urls import *

class Story7TestCase(TestCase):

    def test_url_story7_exist(self):
        response = Client().get('/story-7/')
        self.assertEqual(response.status_code, 200)

    def test_views_index(self):
        found = resolve('/story-7/')
        self.assertEqual(found.func, index)



$(document).ready(function() {
    $(".card-header").click(function(){
        if ($(this).next(".card-body").hasClass("active")) {
            $(this).next(".card-body").removeClass("active").slideUp();
        } else {
            $(".card-body").slideUp();
            $(this).next(".card-body").addClass("active").slideDown();
        }
    })

    $('.switch').on('click', function(){
        var parentDiv = $(this).closest('div.card'),
            dir = $(this).data('sort');

        if (dir === 'up'){
            parentDiv.insertBefore(parentDiv.prev())
        } else if (dir === 'down'){
            parentDiv.insertAfter(parentDiv.next())
        }
    })
})
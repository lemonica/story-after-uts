from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import CreateUserForm

def home(request):
    return render(request, 'story9/welcome.html')

def user_login(request):
    if request.method == "POST":
        username_input = request.POST["username"]
        password_input = request.POST["password"]

        user = authenticate(request, username=username_input, password=password_input)

        if user is not None:
            login(request, user)
            return redirect('story9:home')

    return render(request, 'story9/login.html')

def user_logout(request):
    if request.method == "POST":
        if request.POST["logout"] == "Logout":
            logout(request)
            return redirect('story9:home')

    return render(request, 'story9/logout.html')

def user_signup(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story9:login')
    context = {'form' : form}
    return render(request,'story9/signup.html', context)







from django.test import TestCase, Client
from django.urls import resolve
from .views import *


class Story9UnitTest(TestCase):
    def test_url_home_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_login_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_signup_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_url_logout_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)

    
    def test_views_home(self):
        found = resolve('/')            
        self.assertEqual(found.func, home)

    def test_views_login(self):
        found = resolve('/login/')            
        self.assertEqual(found.func, user_login)

    def test_views_logout(self):
        found = resolve('/logout/')            
        self.assertEqual(found.func, user_logout)

    def test_views_signup(self):
        found = resolve('/signup/')            
        self.assertEqual(found.func, user_signup)



    
    

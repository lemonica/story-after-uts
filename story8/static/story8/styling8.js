$(document).ready(function(){

    // search kalo press enter
    $("#inputan").on("keyup", function(e){
        if (e.which === 13){
            cariBuku();
        }
    })

    // search kalo klik button cari
    $("#search").click(function(){
        cariBuku();
    });

    function cariBuku(){
        var keyword = $("#inputan").val();
        $.ajax({
            url: '/story-8/cari?q=' + keyword,

            success: function(result){
                var obj_result = $('#buku');
                obj_result.empty();
                for(i = 0; i < result.items.length; i++){
                    var judul = result.items[i].volumeInfo.title;
                    var penulis = result.items[i].volumeInfo.authors;
                    var penerbit = result.items[i].volumeInfo.publisher;
                    var cover = result.items[i].volumeInfo.imageLinks.smallThumbnail;
                    var nomor = i+1;
                    obj_result.append("<tr><td>" + nomor + "</td><td>" + judul + "</td><td>" + penulis + "</td><td>" + penerbit + "</td><td><img src='" + cover + "'></td></tr>");
                }
            }
        });
    }
})
from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class Story8UnitTest(TestCase):

    def test_url_story8_exist(self):
        response = Client().get('/story-8/')
        self.assertEqual(response.status_code, 200)

    def test_url_cari_exist(self):
        response = Client().get('/story-8/cari')
        self.assertEqual(response.status_code, 301)

    def test_views_index(self):
        found = resolve('/story-8/')
        self.assertEqual(found.func, index)

    def test_views_search(self):
        found = resolve('/story-8/cari/')
        self.assertEqual(found.func, search)



from django.http import response
from django.shortcuts import render
from django.http import JsonResponse
import requests, json

def index(request):
    response = {}
    return render(request, 'story8/bookfinder.html', response)

def search(request):
    arg = request.GET['q']
    link = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(link)
    data = json.loads(r.content)
    return JsonResponse(data, safe=False)
